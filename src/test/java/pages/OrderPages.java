package pages;

import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import stepDef.Hooks;
import utils.BasePage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static reporting.ComplexReportFactory.getTest;

public class OrderPages extends BasePage {
    WebDriver driver;
    ArrayList<String> old_order = new ArrayList<String>();

    public OrderPages() {
        super("Login Page");
        this.driver = Hooks.driver;
    }


    public void visit() {
        openURL();
    }

    public void acceptCookies(){
        WebElement cookies = findElementsVisibility(By.id("onetrust-accept-btn-handler"));
        if(cookies!=null) {
            click(By.id("onetrust-accept-btn-handler"), "Accept cookies button", 15);
        }
    }
    public void shopNow(){
        WebElement shopNow = findElementsVisibility(By.xpath("//span[text()='SHOP NOW']"));
        if(shopNow!=null){
            click(By.xpath("//span[text()='SHOP NOW']"),"Shop Now button",15);
        }
    }

    public void countrySelector() {
        click(By.xpath("//button[@name='countrySelector']"), "Country selector button", 15);
    }

    public void enterRegion(String region) {
        click(By.xpath("//input[@class='country-search-text-box']"), "Region Search box", 15);
        enter(By.xpath("//input[@class='country-search-text-box']"), region, "Region Search box", 15);
    }

    public void displayedCountries(String country_name) {
        String actualCountryName;
        actualCountryName = getText(By.xpath("//ul[@class='country-list']/li/div[@class='country-name cs-label fwrd-arrow']/a"), 10);

        if (actualCountryName.equals(country_name)) {
           getTest().log(LogStatus.PASS,actualCountryName+"Country name is displayed when choose the"+actualCountryName);
           logger.info(actualCountryName+" Country name is displayed when choose the"+actualCountryName);
        }
        else {
            getTest().log(LogStatus.FAIL,actualCountryName+"Country name is not displayed when choose the"+actualCountryName);
            logger.info(actualCountryName+" Country name is not displayed when choose the"+actualCountryName);

        }
    }

    public void clickCountry(String country_name) {
        Actions action = new Actions(driver);
        WebElement countryName = findElementVisibility(By.xpath("//ul[@class='country-list']//li//div[@class='country-name cs-label fwrd-arrow']//a[text()='" + country_name + "']"), 15);
        action.moveToElement(countryName).perform();

        click(By.xpath("//ul[@class='country-list']//li[@class='multi-language-list-item']//a[text()='(en)']"), "Choose " + country_name, 15);
    }

    public void checkCountry(String currency) {
        if(findElementVisibility(By.xpath("//button[@class='countrySelector']//span[text()='" + currency + "']"), 15).isDisplayed()){
            getTest().log(LogStatus.PASS,currency+" Currency is displayed");
            logger.info(currency+" Currency is displayed");
        }
        else {
            getTest().log(LogStatus.FAIL,currency+" Currency is not displayed");
            takeScreenshot(currency+" Currency is not displayed");
            logger.info(currency+" Currency is not displayed");
        }
    }

    public void countryList() {
        staticWait(5000);
        List<WebElement> webElements = driver.findElements(By.xpath("//div[@class='language-container']"));
        int i = 1;
        for (WebElement ele : webElements) {
            try {
                WebElement country_name = driver.findElement(By.xpath("(//div[@class='country-name cs-label fwrd-arrow'])[" + i + "]"));
                List<WebElement> liCount = driver.findElements(By.xpath("(//div[@class='language-container'])[" + i + "]//ul[@class='multi-language-list display-hide']//li"));
                if (liCount.size() > 1) {
                    ArrayList<String> languages = new ArrayList<String>();
                    String country_names = country_name.getText();
                    int j = 1;
                    for (WebElement langu : liCount) {
                        WebElement li_language = driver.findElement(By.xpath("(//div[@class='language-container'])[" + i + "]//ul[@class='multi-language-list display-hide']//li[" + j + "]//a[@class='multi-language-link for-desktop notranslate']"));
                        languages.add(li_language.getAttribute("text"));
                        j++;
                    }
                    System.out.println(country_names + languages);
                    getTest().log(LogStatus.INFO,country_names + languages);
                    logger.info(country_names + languages);
                }
            } catch (Exception e) {
                System.out.println(e);
            }
            i++;
        }
    }

    public void chooseCategory() {
        Actions action = new Actions(driver);
        WebElement ele = findElementVisibility(By.xpath("//ul[contains(@class,'main-navlist')]//li//a[@title='Collection'][text()='collection']"), 15);
        action.moveToElement(ele).perform();
        click(By.xpath("//a[@href='/en_IN/collection/handbags/_/N-1ddyitx'][text()='View All']"), "view all button", 15);
    }

    public void filters() {
        try {
            click(By.xpath("//button[@class='header']"), "Filter button", 15);
            click(By.xpath("//a[text()='price high to low']"), "Price High to Low", 15);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void sortedList() {
        staticWait(5000);
        List<WebElement> old_lists = driver.findElements(By.xpath("//span[contains(@class,'productAmount')]"));
        for (WebElement list : old_lists) {
            String temp = list.getText();
            temp = temp.replaceAll("[^a-zA-Z0-9]", "");
            old_order.add(temp);
        }
        List<Integer> result = old_order.stream()
                .map(Integer::valueOf)
                .sorted() // sort the elements
                .collect(Collectors.toList());
        Collections.sort(result, Collections.reverseOrder());
        System.out.println(result);
        ArrayList<String> prices = new ArrayList<String>();
        List<WebElement> lists = driver.findElements(By.xpath("//span[contains(@class,'productAmount')]"));
        for (WebElement list : lists) {
            String temp = list.getText();
            temp = temp.replaceAll("[^a-zA-Z0-9]", "");
            prices.add(temp);
        }
        System.out.println(prices);
        List<String> listOne = Arrays.asList(prices.toString());
        List<String> listTwo = Arrays.asList(result.toString());

        if(listOne.equals(listTwo)){
            getTest().log(LogStatus.PASS,"Product list sorted correctly.");
            logger.info("Product list sorted correctly.");
        }
        else {
            getTest().log(LogStatus.FAIL,"Product list not sorted correctly.");
            takeScreenshot("Product list not sorted correctly.");
            logger.info("Product list not sorted correctly.");
        }

    }

}
