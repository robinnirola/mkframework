package stepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pages.OrderPages;

public class OrderSteps {

    OrderPages order = new OrderPages();

    @Given("open the application url")
    public void open_the_application_url() {
        order.visit();
        order.acceptCookies();
        order.shopNow();
    }

    @Then("click on the country selector")
    public void open_country_selector() {
        order.countrySelector();
    }

    @Then("enter the {string}region")
    public void enterTheMalayRegion(String region) {
        order.enterRegion(region);
    }

    @Then("verify countries displayed based on entered region{string}")
    public void verifyCountriesDisplayedBasedOnEnteredRegion(String country_name) {
        order.displayedCountries(country_name);
    }

    @And("click any one country {string}")
    public void clickAnyOneCountry(String country_name) {
        order.clickCountry(country_name);
    }

    @Then("verify selected country is displayed {string}")
    public void verifySelectedCountryIsDisplayed(String currency) {
        order.checkCountry(currency);
    }

    @Then("Check all countries which has more than one language")
    public void check_country_list() {
        order.countryList();
    }

    @Then("navigate to any one category")
    public void choose_category() {
        order.chooseCategory();
    }

    @Then("click on filter button and choose 'High to low'")
    public void filters() {
        order.filters();
    }

    @And("products are listed in the selected sort order")
    public void sorted_list() {
        order.sortedList();
    }
}
