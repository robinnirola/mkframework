Precondition:
    Install Java JDK the latest version Install Intellij Idea community version for editing Java code Download chromedriver place in the workspace https://chromedriver.chromium.org/downloads.
    Download the chromedriver, Firefoxdriver, IEdriver, EDGE driver and place in the root folder of the workspace

To run in Local via Command line:
    gradle cucumber -Denv="local" -Dbname="chrome"
       To run another browser change the Dbname :Eg-Dbname="firefox";-Dbname="edge"

To run in Browserstack Command line:
   1.Find the capabilities in https://www.browserstack.com/automate/capabilities
   2.Update the Browser stack keys in the \src\test\java\stepDef\Driver.java(Ignore already done)
   3.Browser stack website  gradle cucumber -Denv="browserstack" -Dbname="chrome" -DosName="Windows" -DosVersion="10" -Dbrowserversion="latest" -Dresolution="1920x1080"
   4.Browser stack Mobile   gradle cucumber -Denv="browserstackMobile" -Dbname="chrome" "-DdeviceName=Samsung Galaxy S8" "-DdeviceVersion=7.0"

To run the scripts in parallel append the threadcount at end of the line "-DavailableThreadCount=3"
   gradle cucumber -Denv="browserstackMobile" -Dbname="chrome" "-DdeviceName=Samsung Galaxy S8" "-DdeviceVersion=7.0" "-DavailableThreadCount=3"

Reports:
  After the execution go to reports folder and open the .html report to view the result.