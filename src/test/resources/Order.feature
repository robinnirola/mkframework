Feature: Order

  @currencyCheck
  Scenario Outline: Country Currency check for any selected region
    Given open the application url
    Then click on the country selector
     Then enter the "<region>"region
    Then verify countries displayed based on entered region"<country_name>"
    And click any one country "<country_name>"
    Then verify selected country is displayed "<currency>"
    Examples:
      | region | country_name | currency |
      | Malay  | Malaysia     | MY RM    |

  @languageCheck
  Scenario: Country Language options check
    Given open the application url
    Then click on the country selector
    Then Check all countries which has more than one language

  @sortOrder
  Scenario: Products display Sort Order check
    Given open the application url
    Then navigate to any one category
    Then click on filter button and choose 'High to low'
    And products are listed in the selected sort order
