package utils;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import stepDef.Hooks;

import java.io.File;
import java.io.IOException;
import java.util.List;


import static reporting.ComplexReportFactory.getTest;

public class BasePage {
    private WebDriver driver;


    protected static org.apache.log4j.Logger logger;
    private String pageName;
    public static ExtentTest test;

    public BasePage(String pageName) {
        this.driver = Hooks.driver;
        this.pageName = pageName;
        logger = org.apache.log4j.Logger.getLogger(pageName);
    }

    public void openURL() {
        String url = "https://www.michaelkors.co.uk/";
        driver.get(url);
        getTest().log(LogStatus.PASS, "Url opened - " + url);
        logger.info("The application URL is opened");
    }

    public void enter(By by, String value, String name, int time) {
        WebElement element = findElementVisibility(by, time);
        staticWait(200);
        if (element != null) {
            element.clear();
            element.sendKeys(value);
            logger.info(Thread.currentThread().getName() + " - " + name + " entered with value - " + value);
            getTest().log(LogStatus.PASS, name + " entered with value - " + value);
        } else {
            getTest().log(LogStatus.FAIL, pageName + name + " not entered with value - " + value + ", error exist - ");
            logger.info(Thread.currentThread().getName() + " - " + name + " not entered with value - " + value);
            Assert.fail(name + " -  element not present");
        }
    }

    public void click(By by, String name, int time) {
        WebElement element = findElementVisibility(by, time);
        staticWait(200);
        if (element != null) {
            element.click();
            logger.info(Thread.currentThread().getName() + " - " + name + " is clicked");
            getTest().log(LogStatus.PASS, name + " clicked");
        } else {
            getTest().log(LogStatus.FAIL, pageName + name + " not clicked ");
            logger.info(Thread.currentThread().getName() + " - " + name + " is not clicked");
            Assert.fail(name + " -  element not present");

        }
    }


    public String getText(By by, int time) {
        try {
            WebElement ele = findElementVisibility(by, time);
            staticWait(200);
            String getText = ele.getText();
            getTest().log(LogStatus.PASS, " Text displayed is  - " + getText);
            logger.info(" Text displayed is  - " + getText);
            return getText;
        } catch (Exception e) {
            getTest().log(LogStatus.FAIL, "Error Occurred. " + e);
            logger.info("Error Occurred. " + e);
            Assert.fail("" + e);
            return null;
        }
    }

    public WebElement findElementVisibility(final By by, int time) {

        WebDriverWait wait = new WebDriverWait(driver, time);
        staticWait(200);
        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (Exception e) {
            System.out.println();
            return null;
        }

    }

    public void staticWait(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
    }

    public void takeScreenshot(String name) {
        String time = DateTime.now().toString("yyyyMMdd_HHmm");
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("reports\\image" + time + "" + ".png"));
            //String img = test.addScreenCapture(".\\image" + time + "" + ".png");
            String img = getTest().addScreenCapture(".\\image" + time + "" + ".png");

            getTest().log(LogStatus.INFO, "", "Screenshot: " + name + img);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public WebElement findElementsVisibility(final By by) {
        List<WebElement> webElements = driver.findElements(by);
        for (WebElement ele : webElements) {
            try {
                if (ele.isDisplayed()) {
                    return ele;
                }
            } catch (Exception e) {
                System.out.println();
                return null;
            }

        }
        return null;
    }


}
