package stepDef;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Driver {
    public WebDriver driver;
    private static final String USERNAME = "<>";
    private static final String AUTOMATE_KEY = "<>";
    private static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    WebDriver getWebDriver(String browser) throws MalformedURLException {

        if (browser.equalsIgnoreCase("firefox")) {
            String firefoxDownloadDir = System.getProperty("user.dir") + "\\geckodriver.exe";
            FirefoxOptions options = new FirefoxOptions();
            FirefoxProfile profile = new FirefoxProfile();
            profile.setPreference("browser.download.folderList", 2); //Use for the default download directory the last folder specified for a download
            options.setProfile(profile);
            System.setProperty("webdriver.gecko.driver", firefoxDownloadDir);
            driver = new FirefoxDriver(options);
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        }
        else if (browser.equalsIgnoreCase("chrome")) {

            String chromeDownloadDir = System.getProperty("user.dir") + "\\chromedriver.exe";
            HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
            chromePrefs.put("profile.default_content_settings.popups", 0);
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("prefs", chromePrefs);
            System.setProperty("webdriver.chrome.driver", chromeDownloadDir);
            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        }
        else if (browser.equalsIgnoreCase(("IE"))) {
            String downloadFilepath = System.getProperty("user.dir") + "\\src\\main\\resources\\downloadedFiles\\";
            String ieDownloadDir = System.getProperty("user.dir") + "\\IEDriverServer.exe";
            InternetExplorerOptions options = new InternetExplorerOptions();
            System.setProperty("webdriver.edge.driver", ieDownloadDir);
            options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                    true);
            options.setCapability("browser.download.dir", downloadFilepath);
            driver = new EdgeDriver(options);
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        }
        else if (browser.equalsIgnoreCase(("Edge"))) {
            String ieDownloadDir = System.getProperty("user.dir") + "\\msedgedriver.exe";
            EdgeOptions options = new EdgeOptions();
            System.setProperty("webdriver.edge.driver", ieDownloadDir);
            options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                    true);
            driver = new EdgeDriver(options);
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        }
        else{
            Assert.fail("Browser is not initialized");
        }
        return driver;
    }

    WebDriver getBrowserStackDriver(String browser, String os, String OS_version, String browserversion, String resolution) throws MalformedURLException {


        FirefoxProfile profile = null;
        if (browser.equalsIgnoreCase("Chrome")) {
            HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
            chromePrefs.put("profile.default_content_settings.popups", 0);
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("prefs", chromePrefs);
        } else if (browser.equalsIgnoreCase("Firefox")) {
            profile = new FirefoxProfile();
            profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
            profile.setPreference("browser.helperApps.alwaysAsk.force", false);
            profile.setPreference("browser.download.manager.showWhenStarting", false);
            profile.setPreference("pdfjs.disabled", true);
            profile.setPreference("browser.download.folderList", 2);

        }
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("os", os);
        caps.setCapability("os_version", OS_version);
        caps.setCapability("browser", browser);
        caps.setCapability("resolution", resolution);
        caps.setCapability("browser_version", browserversion);
        caps.setCapability("browserstack.local", "false");
        caps.setCapability("browserstack.selenium_version", "3.141.59");

        driver = new RemoteWebDriver(new URL(URL), caps);
        return driver;
    }

    WebDriver getMobileBrowserStackDriver(String browser, String deviceName, String osversion) throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("os_version", osversion);
        caps.setCapability("device", deviceName);
        caps.setCapability("real_mobile", "true");
        caps.setCapability("browserName", browser);
        caps.setCapability("name", "Mobile-Execution");
        return new RemoteWebDriver(new URL(URL), caps);

    }

}
