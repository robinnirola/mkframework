package stepDef;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.cucumber.java.*;


import org.apache.commons.io.FileUtils;

import org.joda.time.DateTime;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


import java.io.File;
import java.io.IOException;

import java.net.MalformedURLException;

import java.util.concurrent.TimeUnit;

import static reporting.ComplexReportFactory.*;


public class Hooks {
    public static WebDriver driver;
    public static ExtentTest test;


    @Before()
    public static void setUp(Scenario scenario) throws MalformedURLException {

        String getTestsExecutor = System.getProperty("bname");


        System.out.println("getTestsExecutor - "+getTestsExecutor);
        System.out.println("------------------------end");
        String environment = System.getProperty("env");

        String browserName = getTestsExecutor;

        String deviceVersion = System.getProperty("deviceVersion");
        String deviceName = System.getProperty("deviceName");
        String osVersion = System.getProperty("osVersion");
        String resolution = System.getProperty("resolution");
        String browserversion = System.getProperty("browserversion");
        String osName = System.getProperty("osName");
        System.out.println(environment);
        System.out.println(browserName);
        System.out.println(deviceVersion);
        System.out.println(deviceName);
        if (environment == null) {
            environment = "local";
            browserName = "chrome";

        }
        if (environment.equalsIgnoreCase("local")) {
            driver = new Driver().getWebDriver(browserName);
            driver.manage().window().maximize();
        } else if (environment.equalsIgnoreCase("browserstack")) {
            driver = new Driver().getBrowserStackDriver(browserName, osName, osVersion, browserversion, resolution);
            driver.manage().window().maximize();
        } else if (environment.equalsIgnoreCase("browserstackMobile")) {
            driver = new Driver().getMobileBrowserStackDriver(browserName, deviceName, deviceVersion);
        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        test = getTest(scenario.getName());


    }

    @After()
    public void Report(Scenario scenario) {
        if (scenario.isFailed()) {
            String time = DateTime.now().toString("yyyyMMdd_HHmm");
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                FileUtils.copyFile(scrFile, new File("reports\\image" + time + "" + ".png"));
                String img = test.addScreenCapture(".\\image" + time + "" + ".png");
                test.log(LogStatus.INFO, "", "Screenshot: " + img);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        closeTest(test);
    }

    @After()
    public void tearDown() {

        if (driver != null) {
            try {
                driver.close();
            } catch (Exception e) {

            }
        }
    }

    @After()
    public void close() {

        closeTest();
        closeReport();
    }


}
